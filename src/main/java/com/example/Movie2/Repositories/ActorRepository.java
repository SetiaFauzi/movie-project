package com.example.Movie2.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Movie2.models.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Long> {

	
}
