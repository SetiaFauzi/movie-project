package com.example.Movie2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.management.relation.Role;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.Repositories.MovieCastRepository;
import com.example.Movie2.modelDTO.MovieCastDTO;
import com.example.Movie2.modelDTO.MovieDTO;
import com.example.Movie2.models.Actor;
import com.example.Movie2.models.Movie;
import com.example.Movie2.models.MovieCast;
import com.example.Movie2.models.MovieCastId;

@RestController
@RequestMapping("/api")
public class MovieCastController {

	@Autowired
	MovieCastRepository movieCastRepository;
	
	@GetMapping("/moviecast/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		ArrayList<MovieCastDTO> listMovieCastDTO = new ArrayList<MovieCastDTO>();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			listMovieCastDTO.add(movieCastDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Movie Cast data succes");
		result.put("Data", listMovieCastDTO);
		
		return result;
		
		
	}
	
	@PostMapping("/moviecast/createDTOMaping")
	public HashMap<String, Object> createMovieCastDTOMaping(@Valid @RequestBody MovieCastDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		MovieCast movieEntity = modelMapper.map(body, MovieCast.class);
		movieCastRepository.save(movieEntity);
		body = modelMapper.map(movieEntity, MovieCastDTO.class);
		body.setId(movieEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Cast data Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/moviecast/updateDTOMaping/{actId}/{movId}")
	public HashMap<String, Object> updateMovieCastDTOMaping(@PathVariable(value = "actId") Long actId, @PathVariable(value = "movId") Long movId,
            @Valid @RequestBody MovieCastDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();

		ModelMapper modelMapper = new ModelMapper();
		
		MovieCastId movieCastId = new MovieCastId();
		movieCastId.setActId(actId);
		movieCastId.setMovId(movId);
		
		ArrayList<MovieCast> listMovieCastEntities = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		for(MovieCast movieCast : listMovieCastEntities) {
			if((movieCast.getId().getActId() == actId) && (movieCast.getId().getMovId() == movId) ) {
				movieCast.setRole(body.getRole());
				 
				movieCastRepository.save(movieCast);
				body =  modelMapper.map(movieCast, MovieCastDTO.class);
				
			}
		
		}
	
		result.put("Status", 200);
		result.put("Message", "Update Movie cast Succes");
		result.put("Data", body);
		
		return result;
	}
	
	
	
	
	
	@DeleteMapping("/moviecast/deleteDTOMaping/{actId}/{movId}")
	public HashMap<String, Object> deleteMovieCastDTOMaping(@PathVariable(value = "actId") Long actId, @PathVariable(value = "movId") Long movId, 
			MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		
		
		MovieCastId movieCastId = new MovieCastId();
		movieCastId.setActId(actId);
		movieCastId.setMovId(movId);
		
		ArrayList<MovieCast> listMovieCastEntities = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		for(MovieCast movieCast : listMovieCastEntities) {
			if((movieCast.getId().getActId() == actId) && (movieCast.getId().getMovId() == movId) ) {
				movieCastRepository.delete(movieCast);
				movieCastDTO =  modelMapper.map(movieCast, MovieCastDTO.class);
		
			}

	}
		result.put("Status", 200);
		result.put("Message", "Delete Movie cast Succes");
		result.put("Data", movieCastDTO);
		
		return result;
		
	}
	
	
	
	
	
}
