package com.example.Movie2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.Repositories.DirectorRepository;
import com.example.Movie2.exceptions.ResourceNotFoundException;
import com.example.Movie2.modelDTO.DirectorDTO;
import com.example.Movie2.models.Director;

@RestController
@RequestMapping("/api")
public class DirectorController {

	@Autowired
	DirectorRepository directorRepository;
	
	
	@GetMapping("/director/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();
		
		for(Director director : listDirectorEntity) {
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			listDirectorDTO.add(directorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Director data succes");
		result.put("Data", listDirectorDTO);
		
		return result;
		
	}
	
	@PostMapping("/director/createDTOMaping")
	public HashMap<String, Object> createDirectorDTOMaping(@Valid @RequestBody DirectorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Director directorEntity = modelMapper.map(body, Director.class);
		directorRepository.save(directorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Director data Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/director/updateDTOMaping/{id}")
	public HashMap<String, Object> updateActorDTOMaping(@PathVariable(value = "id") Long id,
            @Valid @RequestBody DirectorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Director directorEntity = directorRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("director", "id", id));
		
		directorEntity = modelMapper.map(body, Director.class);
		directorEntity.setId(id);
		
		directorRepository.save(directorEntity);
		body = modelMapper.map(directorEntity, DirectorDTO.class);
	
		result.put("Status", 200);
		result.put("Message", "Update Director Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@DeleteMapping("/director/deleteDTOMaping/{id}")
	public HashMap<String, Object> deleteActorDTOMaping(@PathVariable(value = "id") Long id, 
			DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Director directorEntity = directorRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("director", "id", id));
		
		directorEntity = modelMapper.map(directorDTO, Director.class);
		directorEntity.setId(id);
		
		directorRepository.delete(directorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete One of Diretor Data Succes");
		
		return result;
		
		
	}
	
	
	
	
	
}
