package com.example.Movie2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.Movie2.models.Actor;
import com.example.Movie2.modelDTO.ActorDTO;
import com.example.Movie2.Repositories.ActorRepository;
import  com.example.Movie2.exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class ActorController {

	@Autowired
	ActorRepository actorRepository;
	
	@GetMapping("/actor/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();
		
		for(Actor actor : listActorEntity) {
			ActorDTO actorDTO = modelMapper.map(actor, ActorDTO.class);
			listActorDTO.add(actorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read all Actor data succes");
		result.put("Data", listActorDTO);
		
		return result;

	}
	
	@PostMapping("/actor/createDTOMaping")
	public HashMap<String, Object> createActorDTOMaping(@Valid @RequestBody ActorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Actor actorEntity = modelMapper.map(body, Actor.class);
		actorRepository.save(actorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/actor/updateDTOMaping/{id}")
	public HashMap<String, Object> updateActorDTOMaping(@PathVariable(value = "id") Long id,
            @Valid @RequestBody ActorDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Actor actorEntity = actorRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Actor", "id", id));
		
		actorEntity = modelMapper.map(body, Actor.class);
		actorEntity.setId(id);
	
		actorRepository.save(actorEntity);
		body = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Actor Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@DeleteMapping("/actor/deleteDTOMaping/{id}")
	public HashMap<String, Object> deleteActorDTOMaping(@PathVariable(value = "id") Long id,
            ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Actor actorEntity = actorRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Actor", "id", id));
		
		actorEntity = modelMapper.map(actorDTO, Actor.class);
		actorEntity.setId(id);
		
		actorRepository.delete(actorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete One of Author Succes");
		
		return result;
		
	}
	
	
	
	
	
	
	
	
	
	
}
