package com.example.Movie2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.Repositories.GenreRepository;
import com.example.Movie2.exceptions.ResourceNotFoundException;
import com.example.Movie2.modelDTO.GenreDTO;
import com.example.Movie2.models.Genre;

@RestController
@RequestMapping("/api")
public class GenreController {
	
	@Autowired
	GenreRepository genreRepository;
	
	@GetMapping("/genre/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();
		
		for(Genre genre : listGenreEntity) {
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			listGenreDTO.add(genreDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Genre data succes");
		result.put("Data", listGenreDTO);
		
		return result;
		
	}
	
	@PostMapping("/genre/createDTOMaping")
	public HashMap<String, Object> createGenreDTOMaping(@Valid @RequestBody GenreDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Genre genreEntity = modelMapper.map(body, Genre.class);
		genreRepository.save(genreEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Genre data Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/genre/updateDTOMaping/{id}")
	public HashMap<String, Object> updateGenreDTOMaping(@PathVariable(value = "id") Long id,
            @Valid @RequestBody GenreDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Genre genreEntity = genreRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Genre", "id", id));
		
		genreEntity = modelMapper.map(body, Genre.class);
		genreEntity.setId(id);
		
		genreRepository.save(genreEntity);
		body = modelMapper.map(genreEntity, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Genre Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@DeleteMapping("/genre/deleteDTOMaping/{id}")
	public HashMap<String, Object> deleteGenreDTOMaping(@PathVariable(value = "id") Long id, 
			GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Genre genreEntity = genreRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Genre", "id", id));
		
		genreEntity = modelMapper.map(genreDTO, Genre.class);
		genreEntity.setId(id);
		
		genreRepository.delete(genreEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete One of Diretor Data Succes");
		
		return result;
		
	}

	
	
}
