package com.example.Movie2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie2.Repositories.MovieRepository;
import com.example.Movie2.exceptions.ResourceNotFoundException;
import com.example.Movie2.modelDTO.GenreDTO;
import com.example.Movie2.modelDTO.MovieDTO;
import com.example.Movie2.models.Genre;
import com.example.Movie2.models.Movie;

@RestController
@RequestMapping("/api")
public class MovieController {

	@Autowired
	MovieRepository movieRepository;
	
	@GetMapping("/movie/readDTOMaping")
	public HashMap<String, Object>getAllDataMaping(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		ArrayList<MovieDTO> listMovieeDTO = new ArrayList<MovieDTO>();
		
		for(Movie movie : listMovieEntity) {
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			listMovieeDTO.add(movieDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read all Movie data succes");
		result.put("Data", listMovieeDTO);
		
		return result;
	}
	
	@PostMapping("/movie/createDTOMaping")
	public HashMap<String, Object> createMovieDTOMaping(@Valid @RequestBody MovieDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movieeEntity = modelMapper.map(body, Movie.class);
		movieRepository.save(movieeEntity);
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie data Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@PutMapping("/movie/updateDTOMaping/{id}")
	public HashMap<String, Object> updateMovieDTOMaping(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		Movie movieEntity = movieRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Movie", "id", id));
		
		movieEntity = modelMapper.map(body, Movie.class);
		movieEntity.setId(id);
		
		movieRepository.save(movieEntity);
		body = modelMapper.map(movieEntity, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Movie Succes");
		result.put("Data", body);
		
		return result;
		
	}
	
	@DeleteMapping("/movie/deleteDTOMaping/{id}")
	public HashMap<String, Object> deleteMovieeDTOMaping(@PathVariable(value = "id") Long id, 
			MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
	
		Movie movieEntity = movieRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException ("Movie", "id", id));
		
		movieEntity = modelMapper.map(movieDTO, Movie.class);
		movieEntity.setId(id);
		
		movieRepository.delete(movieEntity);

		result.put("Status", 200);
		result.put("Message", "Delete One of Diretor Data Succes");
		
		return result;
	}

	
}
